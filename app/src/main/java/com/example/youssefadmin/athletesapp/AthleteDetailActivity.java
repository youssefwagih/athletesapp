package com.example.youssefadmin.athletesapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class AthleteDetailActivity extends AppCompatActivity {
    private String TAG = AthleteDetailActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    private TextView nameTV;
    private ImageView athleteTV;
    private TextView briefTV;
    private Context context;

    HashMap<String, String> athleteDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_athlete_detail);

        nameTV = (TextView) findViewById(R.id.nameTV);
        briefTV = (TextView) findViewById(R.id.briefTV);
        athleteTV = (ImageView) findViewById(R.id.athleteIV);

        context = getApplicationContext();

        briefTV.setMovementMethod(new ScrollingMovementMethod());

        Intent intent = getIntent();
        nameTV.setText(intent.getStringExtra("name"));
        briefTV.setText(intent.getStringExtra("brief"));

        if (!intent.getStringExtra(("image")).isEmpty())
            Picasso.with(context).load(intent.getStringExtra(("image"))).into(athleteTV);
    }
}
