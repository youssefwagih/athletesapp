package com.example.youssefadmin.athletesapp;

/**
 * Created by Youssef Admin on 4/10/2017.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class AthleteDetailListAdapter extends ArrayAdapter<HashMap<String,String>> {
    Context context;

    public AthleteDetailListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public AthleteDetailListAdapter(Context context, int resource, List<HashMap<String,String>> items) {
        super(context, resource, items);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_item, null);
        }

        HashMap<String, String> athleteDetails = getItem(position);
        TextView nameTV = (TextView) v.findViewById(R.id.nameTV);
        ImageView athleteIV = (ImageView) v.findViewById(R.id.athleteImageView);

        nameTV.setText(athleteDetails.get("name"));
        Log.d("image URL: ", athleteDetails.get("image"));

        if (!athleteDetails.get("image").isEmpty())
            Picasso.with(context).load(athleteDetails.get("image")).transform(new CircleTransform()).into(athleteIV);

        return v;
    }

}
