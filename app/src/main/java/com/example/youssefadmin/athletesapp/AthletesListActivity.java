package com.example.youssefadmin.athletesapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class AthletesListActivity extends AppCompatActivity {
    private String TAG = AthletesListActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    private ListView lv;
    private Context context;
    ArrayList<HashMap<String, String>> atheletesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_athletes_list);

        context = getApplicationContext();
        atheletesList = new ArrayList<>();

        lv = (ListView) findViewById(R.id.list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> selectedAthleteItem = new HashMap<>();
                selectedAthleteItem = atheletesList.get(position);
                Intent intent = new Intent(AthletesListActivity.this, AthleteDetailActivity.class);
                intent.putExtra("name", selectedAthleteItem.get("name"));
                intent.putExtra("brief",selectedAthleteItem.get("brief"));
                intent.putExtra("image",selectedAthleteItem.get("image"));

                startActivity(intent);
            }
        });
        new GetNews().execute();
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetNews extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(AthletesListActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String response = null;

            // URL to get news JSON
            String urlStr = "https://gist.githubusercontent.com/Bassem-Samy/f227855df4d197d3737c304e9377c4d4/raw/ece2a30b16a77ee58091886bf6d3445946e10a23/athletes.josn";
            try {
                URL url = new URL(urlStr);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                // read the response
                InputStream in = new BufferedInputStream(conn.getInputStream());


                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();

                String line;
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append('\n');
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                response = sb.toString();
            } catch (MalformedURLException e) {
                Log.e(TAG, "MalformedURLException: " + e.getMessage());
            } catch (ProtocolException e) {
                Log.e(TAG, "ProtocolException: " + e.getMessage());
            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }

            Log.e(TAG, "Response from url: " + response);

            if (response != null) {
                try {
                    JSONObject jsonObj = new JSONObject(response);

                    // Getting JSON Array node
                    JSONArray athletes = jsonObj.getJSONArray("athletes");

                    // looping through All Contacts
                    for (int i = 0; i < athletes.length(); i++) {
                        JSONObject athlete = athletes.getJSONObject(i);

                        String name = athlete.getString("name");
                        String imageUrl = athlete.getString("image");
                        String brief = athlete.getString("brief");

                        // tmp hash map for single contact
                        HashMap<String, String> athleteHashObject = new HashMap<>();

                        // adding each child node to HashMap key => value
                        athleteHashObject.put("name", name);
                        athleteHashObject.put("image", imageUrl);
                        athleteHashObject.put("brief", brief);

                        // adding contact to contact list
                        atheletesList.add(athleteHashObject);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new AthleteDetailListAdapter(context, R.layout.list_item, atheletesList);

            lv.setAdapter(adapter);
        }

    }
}
